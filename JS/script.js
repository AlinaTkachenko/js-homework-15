/*1.Рекурсія - це коли функція викликає саму себе.
 Рекурсія найкраще проявляє себе коли потрібно повторно викликати одну і туж функцію але з різними параметрами всередині циклу.*/

let numberEntered = prompt('Введіть будь ласка число');

function getNumber(numberEntered) {
     let numberFromEntered = Number(numberEntered);
     while (Number.isNaN(numberFromEntered)) {
          numberEntered = prompt('Введіть будь ласка число', numberEntered);
          numberFromEntered = Number(numberEntered);
     }
     return numberFromEntered;
}

let number = getNumber(numberEntered);
console.log(number);
/*Без рекурсії
function getFactorial(num) {
     let result = 2;
     let a = 3;

     if (num === 1 || num === 0) {
          console.log('1')
          return result = 1;
     } else {
          for (let i = 2; i < num; i++) {
               result = result * a;
               a++;
          }
          return result;
     }
}

let result = getFactorial(number);
alert(result);
*/

let sum = 1;

function getFactorial(num) {
     if (num === 0) return 1;

     sum = sum * num; 
     getFactorial(num - 1); 
}

getFactorial(number);
alert(sum);
